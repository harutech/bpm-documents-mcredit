

CREATE TABLE OPTION
(
    CODE varchar(200) NOT NULL,
    NAME varchar(400) NOT NULL,
    TYPE varchar(100) NOT NULL,
	VALUE varchar(200),
	DESCRIPTION varchar(200),
    STATUS integer,
    OP_ORDER integer
	CONSTRAINT PK_CODE_OPTION PRIMARY KEY (CODE)
);


CREATE TABLE CUSTOMER
(
    CIF varchar(20) NOT NULL,
    CUST_NAME varchar(200),--Họ và tên khach hang
    CUST_DOB date,-- ngay sinh
    CUST_GENDER integer,-- gioi tinh
    PHONE varchar(20),-- so dt
	MOBILE varchar(20),-- so dd
	SIM_TRUE integer,--sim chinh tru hay khong
	SIM_NAME varchar(200),--Ten chu sim
	SIM_RELATIONSHIP varchar(200),-- Moi quan he vs chu sim
	CUST_EDUCATION varchar(200),-- Học vấn
	CUST_MARITAL_STATUS integer,-- Hôn nhan
	CUST_EMAIL varchar(200),--Email 
	CREATED_DATE timestamp without time zone,--Ngay tao
	CONSTRAINT CIF_KEY PRIMARY KEY (CIF)
)
;

CREATE TABLE CUSTOMER_ADDRESS
(
    ID integer NOT NULL,
    CIF varchar(200),-- Mã khach hàng
    ADDRESS varchar(500),-- dia chi
	APARTMENT_NUMBER varchar(100),-- số nhà
    PROVINCE varchar(100),-- Tỉnh
	DISTRICT varchar(100),-- Huyện
	WARD varchar(100),-- Xã
	TYPE varchar(10),-- Loai dia chỉ:1:thuong tru,2:sinh song
	ACCOMMODATION_TYPE varchar(100),--Tình trạng chỗ ở
	LIFETIME_IN_YEAR integer,--Thời gian sống tại địa chỉ hiện tại (năm)
	LIFETIME_IN_MONTH integer,--LIFETIME_IN_MONTH
	CREATED_DATE timestamp without time zone,--Ngày tạo
	CONSTRAINT 	PK_ID_CUSTOMER_ADDRESS PRIMARY KEY (ID)
);
CREATE SEQUENCE CUSTOMER_ADDRESS_ID_SEQ
START 1
INCREMENT 1
OWNED BY CUSTOMER_ADDRESS.ID;

CREATE TABLE FILE_UPLOAD
(
    ID integer NOT NULL,
    CIF varchar(200),
    LOANS_PROFILE_ID integer,
    TYPE varchar(100),
    URL varchar(500),
	NAME varchar(200),
	TYPE_FILE varchar(100),
	CONSTRAINT PK_ID_FILE_UPLOAD PRIMARY KEY (ID)
);
CREATE SEQUENCE FILE_UPLOAD_ID_SEQ
START 1
INCREMENT 1
OWNED BY FILE_UPLOAD.ID;

CREATE TABLE IDENTITY_PAPER --Giay to tuy than cua khach hang
(
    ID integer NOT NULL,
    CIF varchar(200),
    TYPE varchar(100),--Loại giấy tờ tùy thân: the PET hay giay
    CODE varchar(200),--Loại giấy tờ tùy thân = option.code
	NUMBER varchar(200),--So giay to
	IDENTITY_ISSUE_DATE date,--Ngay cap 
	IDENTITY_EXPIRED_DATE  date,--Ngay het han
	IDENTITY_ISSUE_PLACE varchar(500),--Nơi cấp CMND/CCCD/CMQĐ/Hộ chiếu
	REASON_REJECT varchar(200),--Lý do trả về từ thẩm định
    STATUS integer, --Trạng thái giấy tờ
	CREATED_DATE timestamp without time zone,--Ngay tạo
	CONSTRAINT PK_ID_IDENTITY_PAPER PRIMARY KEY (ID)
);
CREATE SEQUENCE IDENTITY_PAPER_ID_SEQ
START 1
INCREMENT 1
OWNED BY IDENTITY_PAPER.ID;



CREATE TABLE IDENTITY_PAPER_OPTION --Danh sach giay to tuy than khach hang
(
    ID integer NOT NULL,
    CIF varchar(200),
    LOANS_PROFILE_ID integer,-- So ho so vay von: Neu la giay to thong tin ca nhan thi de trong.
	TYPE varchar(100),-- Giay to tuy than chinh/thu cap/ dia chi cu tru
    CODE varchar(200),--Loại giấy tờ tùy thân = option.code
	CONSTRAINT PK_ID_IDENTITY_PAPER_OPTION PRIMARY KEY (ID)
);

CREATE SEQUENCE IDENTITY_PAPER_OPTION_ID_SEQ
START 1
INCREMENT 1
OWNED BY IDENTITY_PAPER_OPTION.ID;

CREATE TABLE LOANS_PROFILE
(
    ID integer NOT NULL,
    CIF varchar(200),
    SHOP_CODE varchar(200), -- mã sip
	CUSTOMER_ABILITY_ID integer,--Thong tin kha nang tai chinh cua khach hang
    LOAN_PURPOSE varchar(200),-- mục đích vay
	LOAN_TENOR integer,--thoi gian vay
	LOAN_PROGRAM varchar(500),
	AGENCY varchar(100),-- dai ly
	COUNT_MERCHANDISE integer,-- Tong so luong hang hoa
	COUNT_MERCHANDISE_VALUE double precision,--Tong so gia tri hang hoa
	REPAID_AMOUNT double precision,-- so tien tra truoc
	LOAN_AMT_MINUS_INSU double precision,--khoan vay đề nghị
	HAS_INSURANCE integer,-- tham gia BH
	INSURANCE_COMPANY varchar(500),
	INSURANCE_FEE double precision,
	LOAN_AMOUNT double precision,-- so tien vay bao hiem
	SCORE integer,--thoi gian vay
	NON_SCORE integer,--thoi gian vay
	STATUS integer,-- trang thai ho so
	CREATED_BY varchar(100),-- nguoi tao
	CREATED_DATE timestamp without time zone,-- ngay tao
	UPDATE_BY varchar(100),-- nguoi cap nhat
	UPDATE_DATE timestamp without time zone,-- ngay cap nhat
	CONSTRAINT PK_ID_LOANS_PROFILE PRIMARY KEY (ID)
);

CREATE SEQUENCE LOANS_PROFILE_ID_SEQ
START 1
INCREMENT 1
OWNED BY LOANS_PROFILE.ID;


CREATE TABLE MERCHANDISE
(
    ID integer NOT NULL,
    MODELS varchar(200),--mã hàng hóa
    BRAND varchar(200),--nhãn hiệu
	GOODS_TYPE varchar(100),--loại hàng hóa
	GOODS_PRICE double precision,--giá hàng hóa
	STATUS integer,
	CONSTRAINT PK_ID_MERCHANDISE PRIMARY KEY (ID)
);
CREATE SEQUENCE MERCHANDISE_ID_SEQ
START 1
INCREMENT 1
OWNED BY MERCHANDISE.ID;

CREATE TABLE LOANS_MERCHANDISE
(
    ID integer NOT NULL,
    LOANS_ID integer,
    MERCHANDISE_ID integer,
	GOODS_PRICE double precision,--giá hàng hóa
	STATUS integer,
	CONSTRAINT PK_ID_LOANS_MERCHANDISE PRIMARY KEY (ID)
);
CREATE SEQUENCE LOANS_MERCHANDISE_ID_SEQ
START 1
INCREMENT 1
OWNED BY LOANS_MERCHANDISE.ID;


CREATE TABLE CUSTOMER_CAREER -- Thong tin nghe nghiep khach hang
(
    ID integer NOT NULL,
    CIF varchar(200),
    CUST_PROFESSIONAL varchar(200),-- Nghề nghiệp
    CUST_POSITION varchar(200), -- Chức vụ
	LABOUR_CONTRACT_TYPE varchar(200),-- Loại HĐLĐ
	COMPANY_DEPARTMENT varchar(400),--Phòng ban
	COMPANY_NAME varchar(500),--Tên đơn vị công tác/kinh doanh
	PHONE_NUMBER varchar(100),--Điện thoại công ty
	CUST_CAREER varchar(100),--ngành nghề
	START_TIME date,--ngày bắt đầu làm viêc
	WORKTIME_IN_YEAR integer,--Thời gian công tác (năm)
	WORKTIME_IN_MONTH integer,--Thời gian công tác (tháng)
	APARTMENT_NUMBER varchar(200),--Số nhà
    PROVINCE varchar(100),--Tỉnh
	DISTRICT varchar(100),--Huyện
	WARD varchar(100),-- Xã
	CREATED_DATE timestamp without time zone,
	CONSTRAINT PK_ID_CUSTOMER_CAREER PRIMARY KEY (ID)
	);

CREATE SEQUENCE CUSTOMER_CAREER_ID_SEQ
START 1
INCREMENT 1
OWNED BY CUSTOMER_CAREER.ID;

CREATE TABLE CUSTOMER_ABILITY
(
    ID integer NOT NULL,
    CIF varchar(200),
    LOANS_PROFILE_ID integer,
    CUST_SALARY double precision,--tong thu nhap
    PAYROLL_METHOD varchar(100),--hinh thuc nhan luong
	NO_OF_DEPENDENT varchar(200),-- so nguoi phu thuoc
	CUST_CREDIT_ON_OTHER_BANK integer,--Người đề nghị vay vốn đang có khoản vay tín chấp tại các TCTD 
	CUST_MONTHLY_PAYMENT double precision,--so tien hang thang phai tra
	CREATED_DATE timestamp without time zone,
	CONSTRAINT PK_ID_CUSTOMER_ABILITY PRIMARY KEY (ID)
);

CREATE SEQUENCE CUSTOMER_ABILITY_ID_SEQ
START 1
INCREMENT 1
OWNED BY CUSTOMER_ABILITY.ID;

CREATE TABLE  CUSTOMER_RELATIVES -- Moi quan he voi khach hang
(
    ID integer NOT NULL,
    CIF varchar(200),
    RELATION_SPOUSE varchar(100),-- moi quan he
	SPOUSE_INDENTITY_NUMBER varchar(100),--Số CMND/CCCD
	SPOUSE_NAME varchar(200),-- ho ten
	SPOUSE_DOB DATE,-- ngày sinh
	SEX integer,-- Gioi tính
	SPOUSE_PHONE_NUMBER varchar(100),--Số điện thoại
	MOBILE varchar(100),--Số dd
	COMPANY_NAME varchar(200),--Tên cty
	ADDRESS varchar(500),--Dia chỉ
	ADDRESS_TYPE integer,--Dia chi co giong dia chi khach hang hay ko: 0 va 1
	APARTMENT_NUMBER varchar(200),-- số nhà
	PROVINCE varchar(100),
	DISTRICT varchar(100),
	WARD varchar(100),
	CONTENT_CALL varchar(4000),--Nội dung cuộc gọi thẩm định
	CREATED_DATE timestamp without time zone,
	CONSTRAINT PK_ID_CUSTOMER_RELATIVES PRIMARY KEY (ID)
);

CREATE SEQUENCE CUSTOMER_RELATIVES_ID_SEQ
START 1
INCREMENT 1
OWNED BY CUSTOMER_RELATIVES.ID;


CREATE TABLE  additional_info --Thong tin khác
(
    id integer NOT NULL,
    loans_profile_id integer,
    course_time integer,
	insurance_file integer,
	type_service varchar(100),
	birthday DATE,
	CONSTRAINT PK_ID_additional_info PRIMARY KEY (ID)
);

CREATE SEQUENCE additional_info_id_seq
START 1
INCREMENT 1
OWNED BY add_information.id;

//
CREATE TABLE  Initial_Data -- khoi tao request 
(
    id integer NOT NULL,
    FULL_NAME varchar(200),-- ho va ten
	IDENTITY_NUMBER varchar(100),--so giay to tuy than
	PHONE_NUMBER varchar(100),-- so dt
	CUST_DOB date,-- ngay sinh
	INCOME double precision,-- thu nhap
	LOAN_PURPOSE varchar(200),-- muc dich vay
	AGENCY varchar(100),-- dai ly
	REPAID_AMOUNT double precision,-- so tien tra truoc
	LOAN_AMT_MINUS_INSU double precision,--khoan vay đề nghị
	LOAN_TENOR integer,--thoi gian vay
	COLLATERAL integer,-- tài sản đảm bảo
	CHANNEL varchar(100),--kenh tao
	LOAN_PROGRAM varchar(200),--chương trình tham gia
	HAS_INSURANCE integer,-- tham gia  bao hiểm tín dụng
    CREATED_DATE timestamp without time zone,-- ngay tao
    STATUS integer,-- trang thai
	CONSTRAINT PK_ID_Initial_Data PRIMARY KEY (ID)
);

CREATE SEQUENCE Initial_Data_id_seq
START 1
INCREMENT 1
OWNED BY Initial_Data.id;


CREATE TABLE DC_RESULT
   (
    ID integer NOT NULL,
	LOANS_PROFILE_ID integer,
    CUST_IDENTITY integer, --Nhan dang khach hàng 0-sai,1-dung
    NOTE varchar(4000),-- Ghi chú
	INCOME double precision,-- thu nhap tham dinh
	DTI double precision,--kha nang tra no khach hang dua tren thu nhap
	DC_OPINION  varchar(20),-- ý kiến DC(ma code luu bang option)
	REASON_RETURN varchar(4000),--lý do trả về cho Sale
	REASON_REJECT varchar(4000),--lý do từ chối
	SALE_COMMENT varchar(4000),--Nhận sét cho sale
	COMMENT varchar(4000),--Nhận sét
	STATUS integer,--Trang thái
	CREATED_BY varchar(100),-- nguoi tao
	CREATED_DATE timestamp without time zone,-- ngay tao
	UPDATE_BY varchar(100),-- nguoi cap nhat
	UPDATE_DATE timestamp without time zone,-- ngay cap nhat
	CONSTRAINT PK_ID_DC_RESULT PRIMARY KEY (ID)
	);
	
CREATE SEQUENCE DC_RESULT_ID_SEQ
START 1
INCREMENT 1
OWNED BY DC_RESULT.ID;


CREATE TABLE FV_DATA_INFO --Thông tin thẩm định
(
    ID integer NOT NULL,
    LOANS_PROFILE_ID integer,-- ho so vay
	FV_TYPE varchar(100),--Loại thẩm định : 1-tại nhà ,2-tại công ty,3-tại tự doanh
	IDENTITY_NUMBER varchar(100),--CMND/CCCD
	IDENTITY_NUMBER_OLD varchar(100),--CMND/CCCD cũ
	MILITARY_ID varchar(100),--CM quân đội
	CUST_NAME varchar(200),--Họ và tên khach hang
    CUST_DOB date,-- ngay sinh
    CUST_GENDER integer,-- gioi tinh
	ADDRESS varchar(500),-- Địa chỉ
	ACTUAL_DATE date,-- ngày đi thực tế
	FV_USER_CODE varchar(100),-- mã nhân viên thẩm định
	FV_USER_NAME varchar(100),-- nhân viên thẩm định
	CREATED_BY varchar(100),-- nguoi tao
	CREATED_DATE timestamp without time zone,-- ngay tao
	UPDATE_BY varchar(100),-- nguoi cap nhat
	UPDATE_DATE timestamp without time zone,-- ngay cap nhat
	CONSTRAINT PK_ID_FV_DATA_INFO PRIMARY KEY (ID)
);

CREATE SEQUENCE FV_DATA_INFO_ID_SEQ
START 1
INCREMENT 1
OWNED BY FV_DATA_INFO.ID;


CREATE TABLE FV_RELATIVES --Thẩm định thông tin tham chiếu
(
    ID integer NOT NULL,
    FV_DATA_INFO_ID integer,-- thông tin hô sơ thẩm định
	RELATION_REF_FVH varchar(100) ,--Tên
	REF_FVH_NAME varchar(100),--Mối quan hệ
	REF_FVH_PHONE varchar(100),--Số điện thoại
	SPECIAL_CODE varchar(100),-- Code Đặc biệt
	CODE_FIELD  varchar(100),-- Code TD Field
	FV_NOTE varchar(500),--Code TD Field
    FV_COMMENT varchar(1000),--Nhận xét
	CREATED_BY varchar(100),-- nguoi tao
	CREATED_DATE timestamp without time zone,-- ngay tao
	CONSTRAINT PK_ID_FV_RELATIVES PRIMARY KEY (ID)
);
CREATE SEQUENCE FV_RELATIVES_ID_SEQ
START 1
INCREMENT 1
OWNED BY FV_RELATIVES.ID;



CREATE TABLE FVH_DETAIL --Thông tin thẩm định tại nhà
(
    ID integer NOT NULL,
    FV_DATA_INFO_ID integer,-- thông tin hô sơ thẩm định
	CUST_MARITAL_STATUS integer ,--Tình trạng hôn nhân
	NO_OF_CHILDREN integer,--số con trong gia đình
	NO_OF_DEPENDENT integer,--số con người phụ trong gia đình
	CONFIRM_NEIGHBOUR integer,-- xác nhận hàng xóm
	ACCOMMODATION_TYPE integer,--Tình trạng chỗ ở
    RESIDENCE_TYPE integer,--Hộ khẩu / KT3
    CUST_ADDRESS_STATUS integer,-- Địa chỉ nhà KH: 1- dung,0-sai
    CUST_ADDRESS_AREA_STATUS integer,--nhà khach hàng thuộc khu vực
    LIVING_ADDR_CONFIRMATION integer,-- Xác nhận KH sống ở địa chỉ này 1- có,0-không
    LIVING_TIME integer,-- Thời gian KH cư trú tại địa chỉ xác minh
	HOUSE_TYPE integer,-- Loại nhà ở
	HOUSE_AREA integer,-- Diên tich nhà ở
	FVH_ROAD_DIRECTION varchar(2000),-- Chỉ dẫn đến nhà KH (có sơ đồ đính kèm) 
	HOUSE_DESCRIPTION varchar(2000),-- Mô tả chi tiết về nhà KH
	FVH_MAP integer,-- sơ đồ (lấy theo FILE_UPLOAD_ID)
	FVH_FILE integer,-- File báo cáo thẩm định địa bàn (lấy theo FILE_UPLOAD_ID)
	CREATED_BY varchar(100),-- nguoi tao
	CREATED_DATE timestamp without time zone,-- ngay tao
	CONSTRAINT PK_ID_FVH_DETAIL PRIMARY KEY (ID)
);

CREATE SEQUENCE FVH_DETAIL_ID_SEQ
START 1
INCREMENT 1
OWNED BY FVH_DETAIL.ID;

CREATE TABLE FVO_DETAIL --Thông tin thẩm định tại công ty
(
    ID integer NOT NULL,
    FV_DATA_INFO_ID integer,-- thông tin hô sơ thẩm định
	COMP_ADDR_CONFIRMATION integer ,--Xác nhận khách hàng làm việc tại địa chỉ này:1-có,0-không
	CUST_POSITION varchar(200), -- Chức vụ
	LABOUR_CONTRACT_TYPE varchar(200),-- Loại HĐLĐ
	PAYROLL_METHOD varchar(100),--hinh thuc nhan luong 
	PAYDAY varchar(100),--Những ngày nhận lương
	COMP_OWNERSHIP_STATUS integer,--Tình trạng sở hữu văn phòng của công ty
	COMP_ADDRESS_STATUS integer,--Địa chỉ hoạt động của công ty 1-đúng 0-sai
	COMP_OPERATION_STATUS integer,--Tình trạng hoạt động của công ty
	NO_OF_EMPLOYEE integer,--Số nhân viên làm việc tại địa chỉ xác minh
    COMP_INDUSTRY varchar(100),-- Lĩnh vực kinh doanh của công ty
    COMP_AREA integer,--Tổng diện tích công ty
	FVO_ROAD_DIRECTION varchar(2000),--Chỉ dẫn đến công ty KH (có sơ đồ đính kèm)
	COMP_DESCRIPTION varchar(2000),-- Mô tả chi tiết về công ty KH
	FVO_MAP integer,-- sơ đồ (lấy theo FILE_UPLOAD_ID)
	FVO_FILE integer,-- File báo cáo thẩm định địa bàn (lấy theo FILE_UPLOAD_ID)
	CREATED_BY varchar(100),-- nguoi tao
	CREATED_DATE timestamp without time zone,-- ngay tao
	CONSTRAINT PK_ID_FVO_DETAIL PRIMARY KEY (ID)
);
CREATE SEQUENCE FVO_DETAIL_ID_SEQ
START 1
INCREMENT 1
OWNED BY FVO_DETAIL.ID;


CREATE TABLE FVB_DETAIL --Thông tin thẩm định dia ban tai doanh
(
    ID integer NOT NULL,
    FV_DATA_INFO_ID integer,-- thông tin hô sơ thẩm định
	CUST_MARITAL_STATUS integer ,--Tình trạng hôn nhân
	NO_OF_CHILDREN integer,--số con trong gia đình
	NO_OF_DEPENDENT integer,--số con người phụ trong gia đình
	CONFIRM_NEIGHBOUR integer,-- xác nhận hàng xóm
	ACCOMMODATION_TYPE integer,--Tình trạng chỗ ở
    RESIDENCE_TYPE integer,--Hộ khẩu / KT3
    CUST_ADDRESS_STATUS integer,-- Địa chỉ nhà KH: 1- dung,0-sai
    CUST_ADDRESS_AREA_STATUS integer,--nhà khach hàng thuộc khu vực
    LIVING_ADDR_CONFIRMATION integer,-- Xác nhận KH sống ở địa chỉ này 1- có,0-không
    LIVING_TIME integer,-- Thời gian KH cư trú tại địa chỉ xác minh
	HOUSE_TYPE integer,-- Loại nhà ở
	HOUSE_AREA integer,-- Diên tich nhà ở
	IS_MANAGER integer,-- Là giám đốc: 1:có 0-không
	COMP_OPERATION_STATUS integer,--Tình trạng hoạt động của doanh nghiep
	NO_OF_EMPLOYEE integer,--Số nhân viên làm việc tại địa chỉ xác minh
	WORKTIME_IN_YEAR integer,--năm hoat động
	WORKTIME_IN_MONTH integer,--tháng hoạt động
	BUSINESS varchar(100),--Linh vuc kinh doanh
    LICENSE varchar(100),--giấy phép kinh doanh
	FVB_ROAD_DIRECTION varchar(2000),-- chỉ dẫn đến nhà khách hàng 
	FVB_DESCRIPTION varchar(2000),-- Mô tả chi tiết về doanh nghiệp
	FVB_MAP integer,-- sơ đồ (lấy theo FILE_UPLOAD_ID)
	FVB_FILE integer,-- File báo cáo thẩm định địa bàn (lấy theo FILE_UPLOAD_ID)
	CREATED_BY varchar(100),-- nguoi tao
	CREATED_DATE timestamp without time zone,-- ngay tao
	CONSTRAINT PK_ID_FVB_DETAIL PRIMARY KEY (ID)
);
CREATE SEQUENCE FVB_DETAIL_ID_SEQ
START 1
INCREMENT 1
OWNED BY FVB_DETAIL.ID;

	CREATE TABLE CUSTOMER_VMGS --Thông tin khach hang check VMGS
(
    ID integer NOT NULL,
    CIF varchar(200),-- ma khach hang
	LOANS_PROFILE_ID integer,-- ho so vay
	ID_NUMBER varchar(50),--so cmnd/cccd
	PHONE_NUMBER varchar(50),--so dt
    NAME  varchar(200),--ten khach hang
	SEX varchar(20) ,-- gioi tinh:
	BIRTHDAY date,--ngay sinh
	ID_CARD_DATE date ,--ngay cap
	ID_CARD_ISSUE varchar(400),--noi cap
	HABI_ADDRESS varchar(200),--dia chi
    TAX_CODE varchar(200),
    NUMBER_OF_DEPEND_ON integer,
	PHONE_CHECK_RESULT integer ,-- ket qua check :1-dung 0:sai
	CREATED_DATE timestamp without time zone,-- ngay tao
	CONSTRAINT PK_ID_CUSTOMER_VMGS PRIMARY KEY (ID)
);

CREATE SEQUENCE CUSTOMER_VMGS_ID_SEQ
START 1
INCREMENT 1
OWNED BY CUSTOMER_VMGS.ID;

CREATE TABLE INCOME_CHECK_VMGS --check THU NHAP VMGS
(
    ID integer NOT NULL,
    CIF varchar(200),-- ma khach hang
	LOANS_PROFILE_ID integer,-- ho so vay
	ID_NUMBER varchar(50),--so cmnd/cccd
	PHONE_NUMBER varchar(50),--so dt
    YEAR  integer,--nam
	TOTAL_INCOME double precision ,--thu nhap:
	COMPANY_CODE varchar(50),--ma cong ty
	COMPANY_NAME Varchar(200) ,--ten cong ty
	CREATED_DATE timestamp without time zone,-- ngay tao
	CONSTRAINT PK_ID_CHECK_IMCOME_VMGS PRIMARY KEY (ID)
);

CREATE SEQUENCE INCOME_CHECK_VMGS_ID_SEQ
START 1
INCREMENT 1
OWNED BY INCOME_CHECK_VMGS.ID;

CREATE TABLE CA_RESULT -- Thong tin tham dinh CA
   (
    ID integer NOT NULL,
	LOANS_PROFILE_ID integer,
	DTI double precision,-- ket qua danh gia
    VALUATION varchar(4000),-- Tổng hợp thẩm định
	CALL_DECISION varchar(100),-- Ý kiến của CALL 
	CALL_RETURN_REASON varchar(100),-- Lý do trả về
	CALL_COMMENT_FOR_SALE varchar(4000),--Nhận xét cho sale
	REASON_REJECT varchar(100),--lý do từ chối
	REASON_REJECT_DETAIL varchar(200),--lý do từ chối chi tiết
	COMMENT varchar(4000),--Nhận sét
	CODE_FIELD varchar(200),--Code field TĐĐB
	NOTE_FOR_FV varchar(2000),--Lưu ý cho TĐĐB
	CONTENT_DETAIL varchar(),--// Nội dung xác minh cua tham đinh
	STATUS integer,--Trang thái
	CREATED_BY varchar(100),-- nguoi tao
	CREATED_DATE timestamp without time zone,-- ngay tao
	UPDATE_BY varchar(100),-- nguoi cap nhat
	UPDATE_DATE timestamp without time zone,-- ngay cap nhat
	CONSTRAINT PK_ID_CA_RESULT PRIMARY KEY (ID)
	);
	
CREATE SEQUENCE CA_RESULT_ID_SEQ
START 1
INCREMENT 1
OWNED BY CA_RESULT.ID;

CREATE TABLE AP_RESULT
   (
    ID integer NOT NULL,
  LOANS_PROFILE_ID integer,
  CREDIT_HISTORY varchar(20),-- lich sử tín dụng (ma code luu bang option)
  SCORE varchar(200),
  AP_OPINION  varchar(20),-- ý kiến AP(ma code luu bang option)
  REASON_REJECT varchar(200),--lý do từ chối
  REASON_REJECT_DETAIL varchar(200),--lý do từ chối
  COMMENT varchar(4000),--Nhận sét
  STATUS integer,--Trang thái
  CREATED_BY varchar(100),-- nguoi tao
  CREATED_DATE timestamp without time zone,-- ngay tao
  UPDATE_BY varchar(100),-- nguoi cap nhat
  UPDATE_DATE timestamp without time zone,-- ngay cap nhat
  );
  
CREATE SEQUENCE AP_RESULT_ID_SEQ
START 1
INCREMENT 1
OWNED BY AP_RESULT.ID;
  
  
  CREATE TABLE IDENTITY_PAPER_REJECT --Danh sach giay totu chối
(
    ID integer NOT NULL,
    IDENTITY_PAPER_OPTION_ID integer,--Id giấy tờ lỗi
    REJECT_CODE varchar(200),--Lý do từ chối = option.code
	STATUS integer,--Trang thái
    CREATED_BY varchar(100),-- nguoi tao
    CREATED_DATE timestamp without time zone,-- ngay tao
	CONSTRAINT PK_ID_IDENTITY_PAPER_REJECT PRIMARY KEY (ID)
);
CREATE SEQUENCE IDENTITY_PAPER_REJECT_ID_SEQ
START 1
INCREMENT 1
OWNED BY IDENTITY_PAPER_REJECT.ID;


CREATE TABLE OP_RESULT -- Kết luận vận hành.
   (
    ID integer NOT NULL,
    LOANS_PROFILE_ID integer,
    OP_OPINION varchar(200),-- ý kiến của vận hành
    STATUS integer,--Trang thái
    CREATED_BY varchar(100),-- nguoi tao
    CREATED_DATE timestamp without time zone,-- ngay tao
    UPDATE_BY varchar(100),-- nguoi cap nhat
    UPDATE_DATE timestamp without time zone,-- ngay cap nhat
	CONSTRAINT PK_ID_OP_RESULT PRIMARY KEY (ID)
  );
CREATE SEQUENCE OP_RESULT_ID_SEQ
START 1
INCREMENT 1
OWNED BY OP_RESULT.ID;


CREATE TABLE OP_REASON_REJECT-- Lý do trả về vận hành 
   (
    ID integer NOT NULL,
    OP_RESULT_ID integer,
    OP_REASON varchar(200),-- ma loi
	OP_REASON_DETAIL varchar(200),-- chi tiet
	COMMENT varchar(1000),-- Nhận sét
    STATUS integer,--Trang thái
    CREATED_BY varchar(100),-- nguoi tao
    CREATED_DATE timestamp without time zone,-- ngay tao
	CONSTRAINT PK_ID_OP_REASON_REJECT PRIMARY KEY (ID)
  );
CREATE SEQUENCE OP_REASON_REJECT_ID_SEQ
START 1
INCREMENT 1
OWNED BY OP_REASON_REJECT.ID;


CREATE TABLE REASON_REJECT-- Lý do loi chung
   (
    ID integer NOT NULL,
    REASON_CODE varchar(200),--ma loi
    REASON_NAME varchar(500),-- ten loi
	REASON_TYPE varchar(200),-- loai ma loi
	DESCRIPTION varchar(1000),-- Nhận sét
    STATUS integer,--Trang thái
    CREATED_BY varchar(100),-- nguoi tao
    CREATED_DATE timestamp without time zone,-- ngay tao
	CONSTRAINT PK_ID_REASON_REJECT PRIMARY KEY (ID)
  );
CREATE SEQUENCE REASON_REJECT_ID_SEQ
START 1
INCREMENT 1
OWNED BY REASON_REJECT.ID;
  











