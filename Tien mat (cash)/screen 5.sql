-- Chứng minh thu nhập trực tiếp
insert into option (code, name, type, status, op_order, value, description) values
('TNTT1', N'Sao kê tài khoản lương','TNTT',1,1,1, N'Chứng minh thu nhập trực tiếp' );
insert into option (code, name, type, status, op_order, value, description) values
('TNTT2', N'Xác nhận lương','TNTT',1,2,2, N'Chứng minh thu nhập trực tiếp' );
insert into option (code, name, type, status, op_order, value, description) values
('TNTT3', N'Bảng lương/Xác nhận lương tập thể','TNTT',1,3,3, N'Chứng minh thu nhập trực tiếp' );
insert into option (code, name, type, status, op_order, value, description) values
('TNTT4', N'Sổ lương hưu','TNTT',1,4,4, N'Chứng minh thu nhập trực tiếp' );
insert into option (code, name, type, status, op_order, value, description) values
('TNTT5', N'Biên lai đóng thuế/ngân sách NN','TNTT',1,5,5, N'Chứng minh thu nhập trực tiếp' );
-- Chứng minh thu nhập gián tiếp
insert into option (code, name, type, status, op_order, value, description) values
('TNGT1', N'Sao kê tài khoản ngân hàng','TNGT',1,1,1, N'Chứng minh thu nhập gián tiếp' );
insert into option (code, name, type, status, op_order, value, description)  values
('TNGT2', N'Hóa đơn điện','TNGT',1,2,2, N'Chứng minh thu nhập  gián tiếp' );
insert into option (code, name, type, status, op_order, value, description) values
('TNGT3', N'Hóa đơn tiện ích','TNGT',1,3,3, N'Chứng minh thu nhập gián tiếp' );
insert into option (code, name, type, status, op_order, value, description) values
('TNGT4', N'Hợp đồng BHNT','TNGT',1,4,4, N'Chứng minh thu nhập  gián tiếp' );
insert into option (code, name, type, status, op_order, value, description) values
('TNGT5', N'GCNĐK Xe máy','TNGT',1,5,5, N'Chứng minh thu nhập  gián tiếp' );
insert into option (code, name, type, status, op_order, value, description) values
('TNGT6', N'HĐ vay vốn tại TCTD khác','TNGT',1,6,6, N'Chứng minh thu nhập  gián tiếp' );
insert into option (code, name, type, status, op_order, value, description) values
('TNGT7', N'Giấy tờ khác','TNGT',1,7,7, N'Chứng minh thu nhập  gián tiếp' );